from __future__ import unicode_literals

import errno
import os
import sys
import tempfile
from argparse import ArgumentParser

from flask import Flask, request, abort, send_from_directory
from werkzeug.middleware.proxy_fix import ProxyFix

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    SourceUser, SourceGroup, SourceRoom,
    TemplateSendMessage, ConfirmTemplate, MessageAction,
    ButtonsTemplate, ImageCarouselTemplate, ImageCarouselColumn, URIAction,
    PostbackAction, DatetimePickerAction,
    CarouselTemplate, CarouselColumn, PostbackEvent,
    StickerMessage, StickerSendMessage, LocationMessage, LocationSendMessage,
    ImageMessage, VideoMessage, AudioMessage, FileMessage,
    UnfollowEvent, FollowEvent, JoinEvent, LeaveEvent,
    ImageSendMessage, RichMenu, RichMenuSize, RichMenuArea,
    RichMenuBounds, FlexSendMessage, SeparatorComponent, ButtonComponent,
    SpacerComponent, BubbleContainer, 
    ImageComponent, IconComponent, BoxComponent, TextComponent, 
    CarouselContainer, ImagemapSendMessage, BaseSize, ImagemapAction,
    MessageImagemapAction, ImagemapArea
)

 #  BeaconEvent, CameraRollAction, LocationAction, QuickReply, QuickReplyButton, 

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1, x_proto=1)
# get channel_secret and channel_access_token from your environment variable
channel_secret = os.getenv('LINE_CHANNEL_SECRET')
channel_access_token = os.getenv('LINE_CHANNEL_ACCESS_TOKEN')
if channel_secret is None:
    print('Specify LINE_CHANNEL_SECRET as environment variable.')
    sys.exit(1)
if channel_access_token is None:
    print('Specify LINE_CHANNEL_ACCESS_TOKEN as environment variable.')
    sys.exit(1)

line_bot_api = LineBotApi(channel_access_token)
handler = WebhookHandler(channel_secret)


# ============================ path variable ====================================

static_path = os.path.dirname(__file__)
static_tmp_path = os.path.join(static_path, 'static', 'tmp')
rich_menu_pic_url = os.path.join(static_path, 'static', 'rich_menu.jpg')
menu_danus_pic_url = os.path.join(static_path, 'static', 'danus_pic.jpg')


# ============================ path variable ====================================


# function for create tmp dir for download content
def make_static_tmp_dir():
    try:
        os.makedirs(static_tmp_path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(static_tmp_path):
            pass
        else:
            raise


# ========================== initiate rich_menu =================================

rich1 = RichMenuArea(
            bounds=RichMenuBounds(x=17, y=0, width=2500, height=856),
            action=PostbackAction(label='pembayaran', data='konfirmasi_pembayaran')
        )
rich2 = RichMenuArea(
            bounds=RichMenuBounds(x=17, y=927, width=834, height=735),
            action=PostbackAction(label='menu', data='menu')
        )
rich3 = RichMenuArea(
            bounds=RichMenuBounds(x=884, y=927, width=735, height=735),
            action=PostbackAction(label='pemesanan', data='list_pemesanan')
        )
rich4 = RichMenuArea(
            bounds=RichMenuBounds(x=1672, y=927, width=814, height=735),
            action=URIAction(label='kontak', uri='https://www.instagram.com/azhararnanda/')
        )

rich_menu_to_create = RichMenu(
    size=RichMenuSize(width=2500, height=1686),
    selected=False,
    name="Nata Menu",
    chat_bar_text="Menu", 
    areas=[rich1, rich2, rich3, rich4]
)

rich_menu_id = line_bot_api.create_rich_menu(rich_menu=rich_menu_to_create)
with open(rich_menu_pic_url, 'rb') as f:
    line_bot_api.set_rich_menu_image(rich_menu_id, 'image/jpeg', f)

# ============================= end rich_menu ===================================

# ========================== initiate event_map =================================

imagemap_event = ImagemapSendMessage(
        base_url='https://i.ibb.co/XDtHBGy/menu-event.jpg',
        alt_text='imagemap untuk event',
        base_size=BaseSize(height=1040, width=1040),
        actions=[
            MessageImagemapAction(
                text='\\sound_system',
                area=ImagemapArea(
                    x=0, y=0, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\kursi',
                area=ImagemapArea(
                    x=342, y=0, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\meja',
                area=ImagemapArea(
                    x=684, y=0, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\panggung',
                area=ImagemapArea(
                    x=0, y=357, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\booth_or_stand',
                area=ImagemapArea(
                    x=342, y=357, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\karpet',
                area=ImagemapArea(
                    x=684, y=357, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\backdrop',
                area=ImagemapArea(
                    x=0, y=703, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\lighting',
                area=ImagemapArea(
                    x=342, y=703, width=342, height=330
                )
            ),
            MessageImagemapAction(
                text='\\tenda',
                area=ImagemapArea(
                    x=684, y=703, width=342, height=330
                )
            )
        ]
    ) 

# ============================= end event_map ===================================

# ============================= initiate flex ===================================

def create_bubble():
    bubble = BubbleContainer(
                direction='ltr',
                hero=ImageComponent(
                    url='https://i.ibb.co/Z1sYQj9/Klipsch-Black-Reference-Theater-Pack-5-1-Surround-System-1024x1024.jpg',
                    size='full',
                    aspect_ratio='20:13',
                    aspect_mode='cover',
                    action=URIAction(uri='http://example.com', label='label')
                ),
                body=BoxComponent(
                    layout='vertical',
                    contents=[
                        # title
                        TextComponent(text='Toshiba Sound', weight='bold', size='xl'),
                        # review
                        BoxComponent(
                            layout='baseline',
                            margin='md',
                            contents=[
                                IconComponent(size='sm', url='https://example.com/gold_star.png'),
                                IconComponent(size='sm', url='https://example.com/grey_star.png'),
                                IconComponent(size='sm', url='https://example.com/gold_star.png'),
                                IconComponent(size='sm', url='https://example.com/gold_star.png'),
                                IconComponent(size='sm', url='https://example.com/grey_star.png'),
                                TextComponent(text='4.0', size='sm', color='#999999', margin='md',
                                              flex=0)
                            ]
                        ),
                        # info
                        BoxComponent(
                            layout='vertical',
                            margin='lg',
                            spacing='sm',
                            contents=[
                                BoxComponent(
                                    layout='baseline',
                                    spacing='sm',
                                    contents=[
                                        TextComponent(
                                            text='Place',
                                            color='#aaaaaa',
                                            size='sm',
                                            flex=1
                                        ),
                                        TextComponent(
                                            text='Shinjuku, Tokyo',
                                            wrap=True,
                                            color='#666666',
                                            size='sm',
                                            flex=5
                                        )
                                    ],
                                ),
                                BoxComponent(
                                    layout='baseline',
                                    spacing='sm',
                                    contents=[
                                        TextComponent(
                                            text='Time',
                                            color='#aaaaaa',
                                            size='sm',
                                            flex=1
                                        ),
                                        TextComponent(
                                            text="10:00 - 23:00",
                                            wrap=True,
                                            color='#666666',
                                            size='sm',
                                            flex=5,
                                        ),
                                    ],
                                ),
                            ],
                        )
                    ],
                ),
                footer=BoxComponent(
                    layout='vertical',
                    spacing='sm',
                    contents=[
                        # callAction, separator, websiteAction
                        SpacerComponent(size='sm'),
                        # callAction
                        ButtonComponent(
                            style='link',
                            height='sm',
                            action=URIAction(label='CALL', uri='tel:000000'),
                        ),
                        # separator
                        SeparatorComponent(),
                        # websiteAction
                        ButtonComponent(
                            style='link',
                            height='sm',
                            action=URIAction(label='WEBSITE', uri="https://example.com")
                        )
                    ]
                )
            )

    return bubble

# =================================== end flex ==================================



@app.route("/callback", methods=['POST'])
def callback():
    # Get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # Get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # Handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'



@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    """ Here's all the messages will be handled and processed by the program """

    msg = (event.message.text).lower()
    profile = line_bot_api.get_profile(event.source.user_id)
    line_bot_api.link_rich_menu_to_user(event.source.user_id, rich_menu_id)
    print('ini id user yang menggunakan rich menu\n'+line_bot_api.get_rich_menu_id_of_user(event.source.user_id))


    if 'profile' in msg:
        if isinstance(event.source, SourceUser):
            line_bot_api.reply_message(
                event.reply_token, [
                    TextSendMessage(text='Halo ' + profile.display_name + '!'),
                    TextSendMessage(text='Status message:\n ' + profile.status_message)
                ]
            )
        else:
            line_bot_api.reply_message(
                event.reply_token,
                TextSendMessage(text="Bot can't use profile API without user ID"))

    elif 'bye' in msg:
        if isinstance(event.source, SourceGroup):
            line_bot_api.reply_message(
                event.reply_token, TextSendMessage(text='Sampai jumpa lagi :)'))
            line_bot_api.leave_group(event.source.group_id)
        elif isinstance(event.source, SourceRoom):
            line_bot_api.reply_message(
                event.reply_token, TextSendMessage(text='Sampai jumpa lagi :)'))
            line_bot_api.leave_room(event.source.room_id)
        else:
            line_bot_api.reply_message(
                event.reply_token,
                TextSendMessage(text="Obrolan 1:1 tidak bisa di tinggalkan :("))

    elif msg == 'buttons':
        buttons_template = ButtonsTemplate(
            title='My buttons sample', text='Hello, my buttons', actions=[
                URIAction(label='Go to line.me', uri='https://line.me'),
                PostbackAction(label='ping', data='ping'),
                PostbackAction(label='ping with text', data='ping', text='ping'),
                MessageAction(label='Translate Rice', text='米')
            ])
        template_message = TemplateSendMessage(
            alt_text='Buttons alt text', template=buttons_template)
        line_bot_api.reply_message(event.reply_token, template_message)
    elif msg == 'carousel':
        carousel_template = CarouselTemplate(columns=[
            CarouselColumn(text='hoge1', title='fuga1', actions=[
                URIAction(label='Go to line.me', uri='https://line.me'),
                PostbackAction(label='ping', data='ping')
            ]),
            CarouselColumn(text='hoge2', title='fuga2', actions=[
                PostbackAction(label='ping with text', data='ping', text='ping'),
                MessageAction(label='Translate Rice', text='米')
            ]),
        ])
        template_message = TemplateSendMessage(
            alt_text='Carousel alt text', template=carousel_template)
        line_bot_api.reply_message(event.reply_token, template_message) 

    elif msg == 'confirm':
        confirm_template = ConfirmTemplate(text='Do it?', actions=[
            MessageAction(label='Yes', text='Yes!'),
            MessageAction(label='No', text='No!'),
        ])
        template_message = TemplateSendMessage(
            alt_text='Confirm alt text', template=confirm_template)
        line_bot_api.reply_message(event.reply_token, template_message)

    elif msg == 'image_carousel':
        image_carousel_template = ImageCarouselTemplate(columns=[
            ImageCarouselColumn(image_url='https://via.placeholder.com/1024x1024',
                                action=DatetimePickerAction(label='datetime',
                                                            data='datetime_postback',
                                                            mode='datetime')),
            ImageCarouselColumn(image_url='https://via.placeholder.com/1024x1024',
                                action=DatetimePickerAction(label='date',
                                                            data='date_postback',
                                                            mode='date'))
        ])
        template_message = TemplateSendMessage(
            alt_text='ImageCarousel alt text', template=image_carousel_template)
        line_bot_api.reply_message(event.reply_token, template_message)

    elif 'nonton' in msg:
        line_bot_api.reply_message(
            event.reply_token, 
            StickerSendMessage(
                package_id='1',
                sticker_id='402'
            )
        )
    elif 'kirim' in msg:
        if 'namjoon' in msg:
            line_bot_api.reply_message(
                event.reply_token,[
                    TextSendMessage(
                        text='nih gw kasih!'
                    ),
                    ImageSendMessage(
                        original_content_url='https://i.ibb.co/PmDMx5x/namjoon2.jpg',
                        preview_image_url='https://i.ibb.co/PmDMx5x/namjoon2.jpg'
                    )
                ]
            )
        elif 'jungkook' in msg:
            line_bot_api.reply_message(
                event.reply_token, [
                    TextSendMessage(
                        text='jungkookieee!!'
                    ),
                    ImageSendMessage(
                        original_content_url='https://i.ibb.co/F6WykmC/jungkook.jpg',
                        preview_image_url='https://i.ibb.co/F6WykmC/jungkook.jpg'
                    )
                ]
            )

    elif msg == 'pesan danus':
        bubble = BubbleContainer(
            direction='ltr',
            hero=ImageComponent(
                url='https://example.com/cafe.jpg',
                size='full',
                aspect_ratio='20:13',
                aspect_mode='cover',
                action=URIAction(uri='http://example.com', label='label')
            ),
            body=BoxComponent(
                layout='vertical',
                contents=[
                    # title
                    TextComponent(text='Brown Cafe', weight='bold', size='xl'),
                    # review
                    BoxComponent(
                        layout='baseline',
                        margin='md',
                        contents=[
                            IconComponent(size='sm', url='https://example.com/gold_star.png'),
                            IconComponent(size='sm', url='https://example.com/grey_star.png'),
                            IconComponent(size='sm', url='https://example.com/gold_star.png'),
                            IconComponent(size='sm', url='https://example.com/gold_star.png'),
                            IconComponent(size='sm', url='https://example.com/grey_star.png'),
                            TextComponent(text='4.0', size='sm', color='#999999', margin='md',
                                          flex=0)
                        ]
                    ),
                    # info
                    BoxComponent(
                        layout='vertical',
                        margin='lg',
                        spacing='sm',
                        contents=[
                            BoxComponent(
                                layout='baseline',
                                spacing='sm',
                                contents=[
                                    TextComponent(
                                        text='Place',
                                        color='#aaaaaa',
                                        size='sm',
                                        flex=1
                                    ),
                                    TextComponent(
                                        text='Shinjuku, Tokyo',
                                        wrap=True,
                                        color='#666666',
                                        size='sm',
                                        flex=5
                                    )
                                ],
                            ),
                            BoxComponent(
                                layout='baseline',
                                spacing='sm',
                                contents=[
                                    TextComponent(
                                        text='Time',
                                        color='#aaaaaa',
                                        size='sm',
                                        flex=1
                                    ),
                                    TextComponent(
                                        text="10:00 - 23:00",
                                        wrap=True,
                                        color='#666666',
                                        size='sm',
                                        flex=5,
                                    ),
                                ],
                            ),
                        ],
                    )
                ],
            ),
            footer=BoxComponent(
                layout='vertical',
                spacing='sm',
                contents=[
                    # callAction, separator, websiteAction
                    SpacerComponent(size='sm'),
                    # callAction
                    ButtonComponent(
                        style='link',
                        height='sm',
                        action=URIAction(label='CALL', uri='tel:000000'),
                    ),
                    # separator
                    SeparatorComponent(),
                    # websiteAction
                    ButtonComponent(
                        style='link',
                        height='sm',
                        action=URIAction(label='WEBSITE', uri="https://example.com")
                    )
                ]
            ),
        )
        bubble2 = BubbleContainer(
            direction='ltr',
            hero=ImageComponent(
                url='https://example.com/cafe.jpg',
                size='full',
                aspect_ratio='20:13',
                aspect_mode='cover',
                action=URIAction(uri='http://example.com', label='label')
            ),
            body=BoxComponent(
                layout='vertical',
                contents=[
                    # title
                    TextComponent(text='Brown Cafe', weight='bold', size='xl'),
                    # review
                    BoxComponent(
                        layout='baseline',
                        margin='md',
                        contents=[
                            IconComponent(size='sm', url='https://example.com/gold_star.png'),
                            IconComponent(size='sm', url='https://example.com/grey_star.png'),
                            IconComponent(size='sm', url='https://example.com/gold_star.png'),
                            IconComponent(size='sm', url='https://example.com/gold_star.png'),
                            IconComponent(size='sm', url='https://example.com/grey_star.png'),
                            TextComponent(text='4.0', size='sm', color='#999999', margin='md',
                                          flex=0)
                        ]
                    ),
                    # info
                    BoxComponent(
                        layout='vertical',
                        margin='lg',
                        spacing='sm',
                        contents=[
                            BoxComponent(
                                layout='baseline',
                                spacing='sm',
                                contents=[
                                    TextComponent(
                                        text='Place',
                                        color='#aaaaaa',
                                        size='sm',
                                        flex=1
                                    ),
                                    TextComponent(
                                        text='Shinjuku, Tokyo',
                                        wrap=True,
                                        color='#666666',
                                        size='sm',
                                        flex=5
                                    )
                                ],
                            ),
                            BoxComponent(
                                layout='baseline',
                                spacing='sm',
                                contents=[
                                    TextComponent(
                                        text='Time',
                                        color='#aaaaaa',
                                        size='sm',
                                        flex=1
                                    ),
                                    TextComponent(
                                        text="10:00 - 23:00",
                                        wrap=True,
                                        color='#666666',
                                        size='sm',
                                        flex=5,
                                    ),
                                ],
                            ),
                        ],
                    )
                ],
            ),
            footer=BoxComponent(
                layout='vertical',
                spacing='sm',
                contents=[
                    # callAction, separator, websiteAction
                    SpacerComponent(size='sm'),
                    # callAction
                    ButtonComponent(
                        style='link',
                        height='sm',
                        action=URIAction(label='CALL', uri='tel:000000'),
                    ),
                    # separator
                    SeparatorComponent(),
                    # websiteAction
                    ButtonComponent(
                        style='link',
                        height='sm',
                        action=URIAction(label='WEBSITE', uri="https://example.com")
                    )
                ]
            ),
        )

        carousel = CarouselContainer(contents=[bubble, bubble2])
        message = FlexSendMessage(alt_text="hello", contents=carousel)
        line_bot_api.reply_message(
            event.reply_token, 
            message
        )

    elif '\\sound_system' == msg:
        flex1 = create_bubble()
        flex2 = create_bubble()
        flex3 = create_bubble()
        flex4 = create_bubble()
        carousel = CarouselContainer(contents=[flex1, flex2, flex3, flex4])
        message = FlexSendMessage(alt_text="sound_system_flex", contents=carousel)
        line_bot_api.reply_message(
            event.reply_token,
            message 
        )

    elif 'hello' in msg:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text="Hello " + profile.display_name +'!'))
    elif 'apa kabar' in msg:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text="Baik kok hehe"))
    elif 'bot maker' in msg:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text="Azhar Difa Arnanda :)"))
    else :
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=event.message.text))


@handler.add(PostbackEvent)
def handle_postback(event):
    if event.postback.data == 'ping':
        line_bot_api.reply_message(
            event.reply_token, TextSendMessage(text='pong'))
    elif event.postback.data == 'datetime_postback':
        line_bot_api.reply_message(
            event.reply_token, TextSendMessage(text=event.postback.params['datetime']))
    elif event.postback.data == 'date_postback':
        line_bot_api.reply_message(
            event.reply_token, TextSendMessage(text=event.postback.params['date']))
    elif event.postback.data == 'konfirmasi_pembayaran':
        line_bot_api.reply_message(
            event.reply_token, TextSendMessage(text='Silahkan kirim foto/screenshot pembayaran'))
    elif event.postback.data == 'menu':
        carousel_template = CarouselTemplate(
            columns=[
                CarouselColumn(
                    text='Memenuhi kebutuhan event-mu :)', 
                    title='Event',
                    thumbnail_image_url= 'https://i.ibb.co/6n490MQ/danus-pic.jpg',
                    actions=[
                        PostbackAction(label='lihat daftar', data='list_event')
                    ]
                ),
                CarouselColumn(
                    text='Melengkapi kebutuhan acara-mu :)',
                    title='Acara',
                    thumbnail_image_url= 'https://i.ibb.co/6n490MQ/danus-pic.jpg',
                    actions=[
                        PostbackAction(label='lihat daftar', data='list_acara')
                    ]
                ),
                CarouselColumn(
                    text='Mengisi perut dengan danusan yang yummi (nom)(nom)',
                    title='Danus',
                    thumbnail_image_url= 'https://i.ibb.co/6n490MQ/danus-pic.jpg',
                    actions=[
                        PostbackAction(label='lihat daftar', data='list_danus')
                    ]
                )
            ]
        )
        template_message = TemplateSendMessage(alt_text='Carousel alt text', template=carousel_template)
        text_message = TextSendMessage(text='Silahkan pilih menu')
        line_bot_api.reply_message(event.reply_token, [template_message, text_message])

    elif event.postback.data == 'list_event':
        line_bot_api.reply_message(
            event.reply_token,
            imagemap_event
        )
    elif event.postback.data == 'list_acara':
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text='list dari acara belum ditambahkan')
        )
    elif event.postback.data == 'list_danus':
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text='list dari danus belum ditambahkan')
        )

    elif event.postback.data == 'list_pemesanan':
        print("nyah")

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
